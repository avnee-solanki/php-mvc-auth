<?php

namespace app\controllers;

use app\core\Application;
use app\core\Controller;
use app\core\middlewares\AuthMiddleware;
use app\core\Request;
use app\core\Response;
use app\models\LoginForm;
use app\models\User;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->registerMiddleware(new AuthMiddleware(['profile']));
    }

    /**
     * @param  Request  $request
     * @param  Response  $response
     * @return string
     */
    public function index(Request $request, Response $response): string
    {

        if (Application::isGuest()) {
            $response->redirect('/login');
        }

        return $this->render('index');
    }

    /**
     * @param  Request  $request
     * @return string
     */
    public function login(Request $request): string
    {
        $loginForm = new LoginForm();
        if ($request->getMethod() === 'post') {
            $loginForm->loadData($request->getBody());
            if ($loginForm->validate() && $loginForm->login()) {
                Application::$app->response->redirect('/');
            }
        }
        $this->setLayout('auth');
        return $this->render('login', [
            'model' => $loginForm
        ]);
    }

    /**
     * @param  Request  $request
     * @return string
     */
    public function register(Request $request): string
    {
        $registerModel = new User();
        if ($request->getMethod() === 'post') {
            $registerModel->loadData($request->getBody());
            if ($registerModel->validate() && $registerModel->save()) {
                Application::$app->session->setFlash('success', 'Thanks for registering');
                Application::$app->response->redirect('/');
            }

        }
        $this->setLayout('auth');
        return $this->render('register', [
            'model' => $registerModel
        ]);
    }

    /**
     * @param  Request  $request
     * @param  Response  $response
     * @return void
     */
    public function logout(Request $request, Response $response): void
    {
        Application::$app->logout();
        $response->redirect('/login');
    }
}
