<?php

namespace app\controllers;

use app\core\Application;
use app\core\Controller;
use app\core\middlewares\AuthMiddleware;
use app\core\Request;
use app\core\Response;
use app\models\LoginForm;
use app\models\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->registerMiddleware(new AuthMiddleware(['profile']));
    }

    /**
     * @param  Request  $request
     * @param  Response  $response
     * @return string
     */
    public function index(Request $request, Response $response): string
    {
        if (Application::isGuest()) {
            Application::$app->response->setStatusCode(401);
            return json_encode([
                'error' => [
                    'code' => 401,
                    'message' => 'Unauthorized Access'
                ]
            ]);
        }

        return $this->getAllUsers($request);
    }

    /**
     * @param  Request  $request
     * @return bool|string
     */
    public function getAllUsers(Request $request): bool|string
    {
        $postData = $request->getBody();
        $offset = $postData['start'] ?? 0;
        $limit = $postData['length'] ?? 10;
        $search = $postData['search'] ?? [];
        $searchValue = $search['value'] ?? '';

        $columns = ['firstname', 'lastname', 'email'];
        $result = User::findAll($columns, $searchValue, $limit, $offset);

        return json_encode(
            [
                'data' => $result['data'],
                'recordsTotal' => $result['totalRows'],
                'recordsFiltered' => $result['totalRows']
            ]
        );
    }
}
