<div class="border rounded-5 mt-5 p-3">
    <h3>System Users</h3>
    <hr>
    <table id="usersTable" class="display">
        <thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>

<?php

/** @var $this View */
use app\core\View;
use app\core\Application;

$this->beginSection('scripts');
?>
<script type="text/javascript" charset="utf8" src="<?= Application::$app->getAssetPath('js/datatable.js'); ?>"></script>
<script type="text/javascript" charset="utf8" src="<?= Application::$app->getAssetPath('js/list.js')?>"></script>
<?php  $this->endSection('scripts'); ?>