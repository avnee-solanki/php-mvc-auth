<!doctype html>
<html lang="en">
<?php use app\core\Application; ?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css"  href="<?= Application::$app->getAssetPath('css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?= Application::$app->getAssetPath('css/custom.css'); ?>">
    <title>User Authentication APP</title>
</head>
<body>

<div class="container">
    {{content}}
</div>

<script type="text/javascript" charset="utf8" src="<?= Application::$app->getAssetPath('js/jquery-3.5.1.slim.min.js'); ?>"></script>
<script type="text/javascript" charset="utf8" src="<?= Application::$app->getAssetPath('js/popper.min.js'); ?>"></script>
<script type="text/javascript" charset="utf8" src="<?= Application::$app->getAssetPath('js/bootstrap.min.js'); ?>"></script>
</body>
</html>