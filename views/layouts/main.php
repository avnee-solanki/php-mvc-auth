<!doctype html>
<html lang="en">
<?php use app\core\Application; ?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css"  href="<?= Application::$app->getAssetPath('css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?= Application::$app->getAssetPath('css/datatable.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?= Application::$app->getAssetPath('css/custom.css'); ?>">

    <title><?php echo $this->title ?></title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <?php
            if (Application::isGuest()): ?>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="/login">Login</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="/register">Register</a>
                    </li>
                </ul>
            <?php else: ?>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <span>Welcome <?php echo Application::$app->user->getDisplayName() ?></span>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/logout">Logout</a>
                    </li>
                </ul>
            <?php endif; ?>
        </div>
    </nav>

    <div class="container">
        <?php if (Application::$app->session->getFlash('success')): ?>
            <div class="alert alert-success">
                <p><?php echo Application::$app->session->getFlash('success') ?></p>
            </div>
        <?php endif; ?>
        {{content}}
    </div>
    <script type="text/javascript" charset="utf8" src="<?= Application::$app->getAssetPath('js/jquery-3.5.1.min.js'); ?>"></script>
    <?= $this->section('scripts') ?>
</body>
</html>