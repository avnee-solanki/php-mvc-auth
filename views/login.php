<?php

/** @var $model \app\models\LoginForm */

use app\core\form\Form;

?>
<div class="border rounded-5 login-page page-center">
    <section class="w-100 p-4 d-flex justify-content-center pb-4">
        <h3>Login</h3>
    </section>
    <section class="w-100 p-4 d-flex justify-content-center pb-4">

        <?php $form = Form::begin('', 'post', [
            'class'=> 'loginForm'
        ]) ?>
            <?php echo $form->field($model, 'email') ?>
            <?php echo $form->field($model, 'password')->passwordField() ?>
            <button class="btn btn-success">Submit</button>
        <?php Form::end() ?>
    </section>
    <section class="w-100 p-4 d-flex justify-content-center pb-4">
        Don't have an account?&nbsp;<a href="/register">Register</a>
    </section>
</div>
