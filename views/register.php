<?php
/** @var $model Model */

use app\core\form\Form;
use app\core\Model;

$form = new Form();
?>

<div class="border rounded-5 register-page page-center">
    <section class="w-100 p-4 d-flex justify-content-center pb-4">
        <h3>Register</h3>
    </section>
    <section class="w-100 p-4 d-flex justify-content-center pb-4">
    <?php $form = Form::begin('', 'post', [
        'class'=> 'registerForm'
    ]) ?>
        <?php echo $form->field($model, 'firstname') ?>
        <?php echo $form->field($model, 'lastname') ?>
        <?php echo $form->field($model, 'email') ?>
        <?php echo $form->field($model, 'password')->passwordField() ?>
        <?php echo $form->field($model, 'passwordConfirm')->passwordField() ?>
        <button class="btn btn-success">Submit</button>
    <?php Form::end() ?>
    </section>
    <section class="w-100 p-4 d-flex justify-content-center pb-4">
        Already have an account?&nbsp;<a href="/login">Login</a>
    </section>
</div>
