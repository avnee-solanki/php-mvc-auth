<?php

namespace app\core;

use app\core\db\Database;

class Application
{
    const EVENT_BEFORE_REQUEST = 'beforeRequest';
    const EVENT_AFTER_REQUEST = 'afterRequest';

    /**
     * @var array
     */
    protected array $eventListeners = [];

    /**
     * @var Application
     */
    public static Application $app;
    /**
     * @var string
     */
    public static string $ROOT_DIR;
    /**
     * @var string|mixed
     */
    public string $userClass;
    /**
     * @var string
     */
    public string $layout = 'main';
    /**
     * @var Router
     */
    public Router $router;
    /**
     * @var Request
     */
    public Request $request;
    /**
     * @var Response
     */
    public Response $response;
    /**
     * @var Controller|null
     */
    public ?Controller $controller = null;
    /**
     * @var Database
     */
    public Database $db;
    /**
     * @var Session
     */
    public Session $session;
    /**
     * @var View
     */
    public View $view;
    /**
     * @var UserModel|null
     */
    public ?UserModel $user;

    /**
     * @param  string  $rootDir
     * @param  array  $config
     */
    public function __construct(string $rootDir, array $config)
    {
        $this->user = null;
        $this->userClass = $config['userClass'];
        self::$ROOT_DIR = $rootDir;
        self::$app = $this;
        $this->request = new Request();
        $this->response = new Response();
        $this->router = new Router($this->request, $this->response);
        $this->db = new Database($config['db']);
        $this->session = new Session();
        $this->view = new View();

        $this->initializeUser();
    }

    /**
     * @return void
     */
    private function initializeUser(): void
    {
        $userId = $this->session->get('user');
        if ($userId) {
            $primaryKey = $this->userClass::primaryKey();
            $this->user = $this->userClass::findOne([$primaryKey => $userId]);
        }
    }

    /**
     * @return bool
     */
    public static function isGuest(): bool
    {
        return !self::$app->user;
    }

    /**
     * @param  UserModel  $user
     * @return bool
     */
    public function login(UserModel $user): bool
    {
        $this->user = $user;
        $className = get_class($user);
        $primaryKey = $className::primaryKey();
        $value = $user->{$primaryKey};
        Application::$app->session->set('user', $value);

        return true;
    }

    /**
     * @return void
     */
    public function logout(): void
    {
        $this->user = null;
        self::$app->session->remove('user');
    }

    /**
     * @return void
     */
    public function run(): void
    {
        $this->triggerEvent(self::EVENT_BEFORE_REQUEST);
        try {
            echo $this->router->resolve();
        } catch (\Exception $e) {
            echo $this->router->renderView('_error', [
                'exception' => $e,
            ]);
        }
    }

    /**
     * @param  string  $eventName
     * @return void
     */
    public function triggerEvent(string $eventName): void
    {
        $callbacks = $this->eventListeners[$eventName] ?? [];
        foreach ($callbacks as $callback) {
            call_user_func($callback);
        }
    }

    /**
     * @param  string  $eventName
     * @param  callable  $callback
     * @return void
     */
    public function on(string $eventName, Callable $callback): void
    {
        $this->eventListeners[$eventName][] = $callback;
    }

    /**
     * @param  string  $path
     * @return string
     */
    public function getAssetPath(string $path = ''): string
    {
        $baseUrl = $this->config['base_url'] ?? '';
        return rtrim($baseUrl, '/') . '/' . ltrim($path, '/');
    }
}