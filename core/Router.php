<?php

namespace app\core;

use app\core\exception\NotFoundException;

class Router
{
    /**
     * @var array
     */
    private array $routeMap = [];

    /**
     * @param  Request  $request
     * @param  Response  $response
     */
    public function __construct(private readonly Request $request, private readonly Response $response) {}

    /**
     * @param  string  $url
     * @param  callable|array  $callback
     * @return void
     */
    public function get(string $url, callable|array $callback): void
    {
        $this->routeMap['get'][$url] = $callback;
    }

    /**
     * @param  string  $url
     * @param  callable|array  $callback
     * @return void
     */
    public function post(string $url, callable|array $callback): void
    {
        $this->routeMap['post'][$url] = $callback;
    }

    /**
     * @param  string  $method
     * @return array
     */
    public function getRouteMap(string $method): array
    {
        return $this->routeMap[$method] ?? [];
    }

    /**
     * @return false|mixed
     */
    public function getCallback(): mixed
    {
        $method = $this->request->getMethod();
        $url = $this->request->getUrl();

        $url = trim($url, '/');

        $routes = $this->getRouteMap($method);

        $routeParams = false;

        foreach ($routes as $route => $callback) {
            $route = trim($route, '/');
            $routeNames = [];

            if (!$route) {
                continue;
            }

            if (preg_match_all('/\{(\w+)(:[^}]+)?}/', $route, $matches)) {
                $routeNames = $matches[1];
            }

            $routeRegex = "@^" . preg_replace_callback('/\{\w+(:([^}]+))?}/', fn($m) => isset($m[2]) ? "({$m[2]})" : '(\w+)', $route) . "$@";

            if (preg_match_all($routeRegex, $url, $valueMatches)) {
                $values = [];
                for ($i = 1; $i < count($valueMatches); $i++) {
                    $values[] = $valueMatches[$i][0];
                }
                $routeParams = array_combine($routeNames, $values);

                $this->request->setRouteParams($routeParams);
                return $callback;
            }
        }

        return false;
    }

    /**
     * @throws NotFoundException
     */
    public function resolve()
    {
        $method = $this->request->getMethod();
        $url = $this->request->getUrl();
        $callback = $this->routeMap[$method][$url] ?? false;
        if (!$callback) {

            $callback = $this->getCallback();

            if ($callback === false) {
                throw new NotFoundException();
            }
        }
        if (is_string($callback)) {
            return $this->renderView($callback);
        }
        if (is_array($callback)) {
            /**
             * @var $controller Controller
             */
            $controller = new $callback[0];
            $controller->action = $callback[1];
            Application::$app->controller = $controller;
            $middlewares = $controller->getMiddlewares();
            foreach ($middlewares as $middleware) {
                $middleware->execute();
            }
            $callback[0] = $controller;
        }
        return call_user_func($callback, $this->request, $this->response);
    }

    /**
     * @param $view
     * @param  array  $params
     * @return bool|array|string
     */
    public function renderView($view, array $params = []): bool|array|string
    {
        return Application::$app->view->renderView($view, $params);
    }

    /**
     * @param $view
     * @param  array  $params
     * @return bool|string
     */
    public function renderViewOnly($view, array $params = []): bool|string
    {
        return Application::$app->view->renderViewOnly($view, $params);
    }
}
