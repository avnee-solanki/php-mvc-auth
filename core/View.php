<?php

namespace app\core;

class View
{
    /**
     * @var array
     */
    protected array $sections = [];

    /**
     * @param  string  $title
     */
    public function __construct(
        public string $title = ''
    ) {}

    /**
     * @param $key
     * @return void
     */
    public function beginSection($key): void
    {
        ob_start();
        $this->sections[$key] = '';
    }

    /**
     * @param $key
     * @return void
     */
    public function endSection($key): void
    {
        $this->sections[$key] = ob_get_clean();
    }

    /**
     * @param  string  $key
     * @return mixed|string
     */
    public function section(string $key): mixed
    {
        return $this->sections[$key] ?? '';
    }

    /**
     * @param $view
     * @param  array  $params
     * @return array|bool|string
     */
    public function renderView($view, array $params): array|bool|string
    {
        $layoutName = Application::$app->layout;
        if (Application::$app->controller) {
            $layoutName = Application::$app->controller->layout;
        }
        $viewContent = $this->renderViewOnly($view, $params);
        ob_start();
        include_once Application::$ROOT_DIR."/views/layouts/$layoutName.php";
        $layoutContent = ob_get_clean();
        return str_replace('{{content}}', $viewContent, $layoutContent);
    }

    /**
     * @param $view
     * @param  array  $params
     * @return bool|string
     */
    public function renderViewOnly($view, array $params): bool|string
    {
        foreach ($params as $key => $value) {
            $$key = $value;
        }
        ob_start();
        include_once Application::$ROOT_DIR."/views/$view.php";
        return ob_get_clean();
    }
}