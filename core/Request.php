<?php
namespace app\core;

class Request
{
    public const POST = 'post';
    public const GET = 'get';

    /**
     * @var array
     */
    private array $routeParams = [];

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return strtolower($_SERVER['REQUEST_METHOD']);
    }

    /**
     * @return mixed
     */
    public function getUrl(): mixed
    {
        $path = $_SERVER['REQUEST_URI'];
        $position = strpos($path, '?');
        if ($position !== false) {
            $path = substr($path, 0, $position);
        }
        return $path;
    }

    /**
     * @return bool
     */
    public function isGet(): bool
    {
        return $this->getMethod() === self::GET;
    }

    /**
     * @return bool
     */
    public function isPost(): bool
    {
        return $this->getMethod() === self::POST;
    }

    /**
     * @return array
     */
    public function getBody(): array
    {
        $data = [];
        if ($this->isGet()) {
            foreach ($_GET as $key => $value) {
                $data[$key] = filter_var($value, FILTER_SANITIZE_SPECIAL_CHARS);
            }
        }
        if ($this->isPost()) {
            $data = $this->sanitizePostData($_POST);
        }
        return $data;
    }

    /**
     * @param  mixed $data
     * @return mixed
     */
    private function sanitizePostData(mixed $data): mixed
    {
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $data[$key] = $this->sanitizePostData($value);
            }
        } else {
            $data = filter_var($data, FILTER_SANITIZE_SPECIAL_CHARS);
        }
        return $data;
    }

    /**
     * @param $params
     * @return static
     */
    public function setRouteParams($params): static
    {
        $this->routeParams = $params;
        return $this;
    }

    /**
     * @return array
     */
    public function getRouteParams(): array
    {
        return $this->routeParams;
    }

    /**
     * @param $param
     * @param $default
     * @return mixed|null
     */
    public function getRouteParam($param, $default = null): mixed
    {
        return $this->routeParams[$param] ?? $default;
    }
}
