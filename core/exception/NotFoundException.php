<?php

namespace app\core\exception;

class NotFoundException extends \Exception
{
    /**
     * @var string
     */
    protected $message = 'Page not found';
    /**
     * @var int
     */
    protected $code = 404;
}