<?php

namespace app\core\db;

use app\core\Application;
use app\core\Model;
use PDOStatement;

abstract class DbModel extends Model
{
    /**
     * @return string
     */
    abstract public static function tableName(): string;

    /**
     * @return string
     */
    public static function primaryKey(): string
    {
        return 'id';
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        $tableName = $this->tableName();
        $attributes = $this->attributes();
        $params = array_map(fn($attr) => ":$attr", $attributes);
        $statement = self::prepare("INSERT INTO $tableName (" . implode(",", $attributes) . ") 
                VALUES (" . implode(",", $params) . ")");
        foreach ($attributes as $attribute) {
            $statement->bindValue(":$attribute", $this->{$attribute});
        }
        $statement->execute();
        return true;
    }

    /**
     * @param $sql
     * @return PDOStatement
     */
    public static function prepare($sql): PDOStatement
    {
        return Application::$app->db->prepare($sql);
    }

    /**
     * @param $where
     * @return DbModel|false|object|\stdClass|null
     */
    public static function findOne($where)
    {
        $tableName = static::tableName();
        $attributes = array_keys($where);
        $sql = implode("AND", array_map(fn($attr) => "$attr = :$attr", $attributes));
        $statement = self::prepare("SELECT * FROM $tableName WHERE $sql");
        foreach ($where as $key => $item) {
            $statement->bindValue(":$key", $item);
        }
        $statement->execute();
        return $statement->fetchObject(static::class);
    }

    /**
     * @param  array  $columns
     * @param  string  $searchValue
     * @param  array  $orderBy
     * @param  int  $limit
     * @param  int  $offset
     * @return bool|array
     */
    public static function findAll(
        array $columns = ['*'],
        string $searchValue = '',
        int $limit = 10,
        int $offset = 0): array
    {
        $tableName = static::tableName();
        $columnString = implode(", ", $columns);

        $query = "SELECT $columnString FROM $tableName";

        $countQuery = "SELECT COUNT(*) FROM $tableName";

        $filterClauses = [];
        foreach ($columns as $column) {
            $filterClauses[] = "$column LIKE :filter_$column";
        }

        if (!empty($filterClauses)) {
            $query .= " WHERE " . implode(" OR ", $filterClauses);
            $countQuery .= " WHERE " . implode(" OR ", $filterClauses);
        }

        $query .= " LIMIT :limit OFFSET :offset";

        $statement = self::prepare($query);

        $countStatement = self::prepare($countQuery);

        foreach ($columns as $column) {
            $statement->bindValue(":filter_$column", '%' . $searchValue . '%');

            $countStatement->bindValue(":filter_$column", '%' . $searchValue . '%');
        }

        $statement->bindValue(':limit', $limit, \PDO::PARAM_INT);
        $statement->bindValue(':offset', $offset, \PDO::PARAM_INT);
        $statement->execute();
        $data = $statement->fetchAll(\PDO::FETCH_CLASS);

        $countStatement->execute();
        $totalRows = $countStatement->fetchColumn();

        return ['data' => $data, 'totalRows' => $totalRows];
    }
}