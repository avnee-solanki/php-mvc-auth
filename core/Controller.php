<?php
namespace app\core;

use app\core\middlewares\BaseMiddleware;

class Controller
{
    /**
     * @var string
     */
    public string $layout = 'main';
    /**
     * @var string
     */
    public string $action = '';

    /**
     * @var array
     */
    protected array $middlewares = [];

    /**
     * @param $layout
     * @return void
     */
    public function setLayout($layout): void
    {
        $this->layout = $layout;
    }

    /**
     * @param $view
     * @param  array  $params
     * @return string
     */
    public function render($view, array $params = []): string
    {
        return Application::$app->router->renderView($view, $params);
    }

    /**
     * @param  BaseMiddleware  $middleware
     * @return void
     */
    public function registerMiddleware(BaseMiddleware $middleware): void
    {
        $this->middlewares[] = $middleware;
    }

    /**
     * @return array
     */
    public function getMiddlewares(): array
    {
        return $this->middlewares;
    }
}