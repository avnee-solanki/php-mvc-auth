<?php

namespace app\core\form;

use app\core\Model;

class Form
{
    /**
     * @param $action
     * @param $method
     * @param $options
     * @return Form
     */
    public static function begin($action, $method, $options = []): Form
    {
        $attributes = [];
        foreach ($options as $key => $value) {
            $attributes[] = "$key=\"$value\"";
        }
        echo sprintf('<form action="%s" method="%s" %s>', $action, $method, implode(" ", $attributes));
        return new Form();
    }

    /**
     * @return void
     */
    public static function end(): void
    {
        echo '</form>';
    }

    /**
     * @param  Model  $model
     * @param $attribute
     * @return Field
     */
    public function field(Model $model, $attribute): Field
    {
        return new Field($model, $attribute);
    }
}