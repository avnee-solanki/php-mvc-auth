<?php

namespace app\core\form;

use app\core\Model;

abstract class BaseField
{
    /**
     * @var string
     */
    public string $type;

    /**
     * Field constructor.
     *
     * @param  Model  $model
     * @param string  $attribute
     */
    public function __construct(public Model $model, public string $attribute) {}

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('<div class="form-group">
                <label>%s</label>
                %s
                <div class="invalid-feedback">
                    %s
                </div>
            </div>',
            $this->model->getLabel($this->attribute),
            $this->renderInput(),
            $this->model->getFirstError($this->attribute)
        );
    }

    abstract public function renderInput();
}