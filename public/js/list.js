$.fn.dataTable.ext.errMode = 'none';
$(document).ready( function () {
    loadUsers();
});

function loadUsers() {
    let limit = $("#usersTable_length select").val();
    $('#usersTable')
        .on( 'error.dt', function ( e, settings, techNote, message ) {
            console.log( 'An error has been reported by DataTables: ', message );
        } )
        .DataTable({
        "serverSide": true,
        "ajax": {
            url: "/api/users",
            type: "POST"
        },
        "columns": [
            { "data": "firstname" },
            { "data": "lastname" },
            { "data": "email" }
        ],
        "length": limit
    });
}