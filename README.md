# PHP MVC framework
Basic custom php mvc framework.

------
## Installation using docker
Make sure you have [docker](https://docs.docker.com/desktop/) installed. <br>
Make sure `docker` and `docker-compose` commands are available in command line.

1. Clone the project using git
2. Copy `.env.example` into `.env` (Don't need to change anything for local development)
3. Navigate to the project root directory and run `docker-compose up -d`
4. Install dependencies - `docker-compose exec app composer install`
5. Run migrations - `docker-compose exec app php migrations.php`
6. Open in browser http://localhost:8092
